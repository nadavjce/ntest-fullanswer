import { ArticlesService } from './../articles.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-classtified-collection',
  templateUrl: './classtified-collection.component.html',
  styleUrls: ['./classtified-collection.component.css']
})
export class ClasstifiedCollectionComponent implements OnInit {

  articles$ : Observable<any>; 
  articles: any; 
  userID;

  constructor(private articleService:ArticlesService,
              public authService: AuthService,
              ) 
              { }

  ngOnInit() 
  {
    this.authService.getUser().subscribe(
      user =>
      {
        this.userID=user.uid;
        console.log("The userID: ",this.userID)
        this.articles$= this.articleService.getArticles(this.userID);
      }
     )
  }

  deleteArticle(key_id:string)
  {
    console.log("In - books.ts deleteBook() ")
    console.log("this is the key_id: ",key_id)
    console.log("this is the userID: ",this.userID)
    this.articleService.deleteArticleFromCollection(this.userID,key_id);
  }

}
